/* 
 **
 ** Copyright 2014, Jules White
 **
 ** 
 */
package com.example.john.capstoneclient02;

import android.content.Context;
import android.content.Intent;

import com.example.john.capstoneclient02.oauth.SecuredRestBuilder;
import com.example.john.capstoneclient02.unsafe.EasyHttpClient;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;

public class VideoSvc {

	public static final String CLIENT_ID = "mobile";

	private static com.example.john.capstoneclient02.VideoSvcApi videoSvc_;

	public static synchronized com.example.john.capstoneclient02.VideoSvcApi getOrShowLogin(Context ctx) {
		if (videoSvc_ != null) {
			return videoSvc_;
		} else {
			Intent i = new Intent(ctx, LoginScreenActivity.class);
			ctx.startActivity(i);
			return null;
		}
	}

	public static synchronized com.example.john.capstoneclient02.VideoSvcApi init(String server, String user,
			String pass) {

//        .setLoginEndpoint(server + com.example.john.capstoneclient02.VideoSvcApi.TOKEN_PATH)

		videoSvc_ = new SecuredRestBuilder()
				.setLoginEndpoint(server + "/login")
				.setUsername(user)
				.setPassword(pass)
				.setClientId(CLIENT_ID)
				.setClient(
						new ApacheClient(new EasyHttpClient()))
				.setEndpoint(server).setLogLevel(LogLevel.FULL).build()
				.create(com.example.john.capstoneclient02.VideoSvcApi.class);

		return videoSvc_;
	}
}
