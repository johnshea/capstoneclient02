package com.example.john.capstoneclient02;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.john.capstoneclient02.gamelogic.GameStateManager;
import com.example.john.capstoneclient02.gamelogic.RatingsDialogFragment;
import com.example.john.capstoneclient02.gamelogic.SetManager;
import com.example.john.capstoneclient02.gamelogic.SpringBasedSetManager;
import com.google.common.base.Strings;


public class PlayGameActivity extends Activity implements RatingsDialogFragment.RatingsDialogListener {

    protected TextView textViewTitle;
    protected TextView textViewRoundTitle;
    protected TextView textViewSetTitle;
    protected TextView textViewStrikeStatus;
    protected TextView textViewScore;
    protected TextView textViewPossiblePoints;

    protected Button btnChoice01;
    protected Button btnChoice02;
    protected Button btnChoice03;
    protected Button btnChoice04;

    protected Set currentSet;

    protected SetManager setManager;

    protected GameStateManager gameStateManager;

    protected boolean hasLives;
    protected int currentRound;
    protected int currentPoints;
    protected int possiblePoints;
    protected int strikesEarned;
    protected int currentScore;

    protected User currentUser;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            Bundle bundle = msg.getData();
            int returnStatus = bundle.getInt("status");

            switch(returnStatus) {
                case 0:
                    initGameVariables();
                    initRoundVariables();
                    initScreen();
                    updateScreenValues();

                    View.OnClickListener buttonClicker = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Button pressedButton = (Button) view;

                            if(setManager.isCorrectAnswer(pressedButton.getText().toString())) {
                                gameStateManager.incrementScore();
//                    Toast.makeText(getApplicationContext(), "Correct! You earned " + gameStateManager.getCurrentPointsValue() + " points.", Toast.LENGTH_SHORT).show();

                                StringBuilder stringBuilder = new StringBuilder();
                                stringBuilder.append("Correct answer was \""
                                        + setManager.getAnswer() + "\" because it is \"" + setManager.getExplanation() + "\".");

                                DialogFragment alertDialog = new RatingsDialogFragment().newInstance(setManager.getCurrentSetNumber(),
                                        stringBuilder.toString());
                                alertDialog.show(getFragmentManager(), "ratings");

                                gameStateManager.incrementRound();

                                setManager.nextSet();
                                currentRound++;
                                initRoundVariables();
                                initScreen();
                                gameStateManager.resetPossiblePoints();

                            } else {
                                gameStateManager.decrementPointValue();
                                gameStateManager.incrementStrikesEarned();
//                    Toast.makeText(getApplicationContext(), "Wrong - The correct answer is now worth " +
//                            gameStateManager.getCurrentPointsValue() + " points.", Toast.LENGTH_SHORT).show();
                                view.setEnabled(false);
                            }

                            if (gameStateManager.isGameOver()) {
                                //Toast.makeText(getApplicationContext(), "Game Over!", Toast.LENGTH_SHORT).show();
                                // save current level
                                final UserSvcApi svc = UserSvc.getOrShowLogin(getApplicationContext());
                                currentUser.setCurrentLevel(setManager.getCurrentSetNumber());
                                currentUser.setScore(gameStateManager.getCurrentScore());

                                new mySaveUserTask().execute();

                                Intent intent = new Intent(getApplicationContext(), GameOverActivity.class);

                                Bundle bundle = new Bundle();

                                bundle.putLong("id", currentUser.getId());
                                bundle.putString("userId", currentUser.getUserid());
                                bundle.putString("firstName", currentUser.getFirstName());
                                bundle.putString("lastName", currentUser.getLastName());
                                bundle.putLong("currentLevel", currentUser.getCurrentLevel());
                                bundle.putInt("score", (int)gameStateManager.getCurrentScore());
                                bundle.putInt("currentRound", gameStateManager.getCurrentRound());
                                bundle.putInt("roundsToPlay", gameStateManager.getRoundsToPlay());
                                bundle.putInt("strikesEarned", gameStateManager.getStrikesEarned());
                                bundle.putInt("maxStrikesAllowed", gameStateManager.getMaxStrikesAllowed());

                                intent.putExtras(bundle);

                                startActivity(intent);

                            }

                            updateScreenValues();
                        }
                    };

                    btnChoice01.setOnClickListener(buttonClicker);
                    btnChoice02.setOnClickListener(buttonClicker);
                    btnChoice03.setOnClickListener(buttonClicker);
                    btnChoice04.setOnClickListener(buttonClicker);
                    break;

                case 1:
                    updateScreenValues();
                    initScreen();
                    break;

            }

        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);

        btnChoice01 = (Button) findViewById(R.id.btnChoice1);
        btnChoice02 = (Button) findViewById(R.id.btnChoice2);
        btnChoice03 = (Button) findViewById(R.id.btnChoice3);
        btnChoice04 = (Button) findViewById(R.id.btnChoice4);

        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewRoundTitle = (TextView) findViewById(R.id.textViewRoundTitle);
        textViewSetTitle = (TextView) findViewById(R.id.textViewSetTitle);

        textViewPossiblePoints = (TextView) findViewById(R.id.textViewPossiblePoints);
        textViewScore = (TextView) findViewById(R.id.textViewScore);
        textViewStrikeStatus = (TextView) findViewById(R.id.textViewStrikeStatus);

        Bundle bundle = this.getIntent().getExtras();

        currentUser = new User();

        currentUser.setId(bundle.getLong("id"));
        currentUser.setUserid(bundle.getString("userId"));
        currentUser.setFirstName(bundle.getString("firstName"));
        currentUser.setLastName(bundle.getString("lastName"));
        currentUser.setCurrentLevel(bundle.getLong("currentLevel"));
        currentUser.setScore(bundle.getLong("score"));

        gameStateManager = new GameStateManager();

//        setManager = new InMemorySetManager();
        setManager = new SpringBasedSetManager(getApplicationContext(), handler);
        setManager.init();
        setManager.setCurrentSet((int) currentUser.getCurrentLevel());

    }

    private void updateScreenValues() {
        textViewRoundTitle.setText("Round " + gameStateManager.getCurrentRound() + " of " + gameStateManager.getRoundsToPlay());
        textViewSetTitle.setText("Set " + setManager.getCurrentSetNumber());
        textViewPossiblePoints.setText("Possible points: " + gameStateManager.getCurrentPointsValue());
        textViewScore.setText("Current Score: " + gameStateManager.getCurrentScore());
        String strikeStatus;
        strikeStatus = Strings.repeat("X", gameStateManager.getStrikesEarned());
        textViewStrikeStatus.setText("Strikes: " + strikeStatus);
    }

    private void initScreen() {
        btnChoice01.setText(setManager.getChoice01());
        btnChoice02.setText(setManager.getChoice02());
        btnChoice03.setText(setManager.getChoice03());
        btnChoice04.setText(setManager.getChoice04());
    }

    private void initRoundVariables() {
        possiblePoints = gameStateManager.getFullPointsForCorrectAnswer();
        btnChoice01.setEnabled(true);
        btnChoice02.setEnabled(true);
        btnChoice03.setEnabled(true);
        btnChoice04.setEnabled(true);
    }

    private void initGameVariables() {
        currentRound = 1;
        currentScore = 0;
        hasLives = true;
        strikesEarned = 0;
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, Integer setNumber) {
//        Toast.makeText(getApplicationContext(), "User liked it (Set " + setNumber + ")", Toast.LENGTH_SHORT).show();

        Rating newRating;
        newRating = new Rating(setNumber, currentUser.getId(), 1);

        new mySaveNewRatingTask().execute(newRating);

    }

    class mySaveNewRatingTask extends AsyncTask<Rating, Void, String> {

        @Override
        protected String doInBackground(Rating... ratings) {
            final UserSvcApi svc = UserSvc.getOrShowLogin(getApplicationContext());

            StringBuilder result;
            result = new StringBuilder();


            Boolean saved;
            saved = svc.addRating(ratings[0]);

            long setId;
            setId = ratings[0].getSetid();
            result.append("Saved new rating (Set " + setId + ").");

            RatingStat currentRatingStat;
            currentRatingStat = svc.getOneRatingStat(setId);

            if((currentRatingStat.getPlayedCount() >= 5) && (currentRatingStat.getAverageRating() <= 0)) {
                svc.disableOneSet(setId);
                result.append("Disabled (Set " + setId + ").");
            }

            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {
//            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog, Integer setNumber) {
//        Toast.makeText(getApplicationContext(), "User hated it (Set " + setNumber + ")", Toast.LENGTH_SHORT).show();

        Rating newRating;
        newRating = new Rating(setNumber, currentUser.getId(), -1);

        new mySaveNewRatingTask().execute(newRating);
    }

    class mySaveUserTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            final UserSvcApi svc = UserSvc.getOrShowLogin(getApplicationContext());

            Boolean saved;
            saved = svc.setInfo(currentUser);

            return saved;
        };

        @Override
        protected void onPostExecute(Boolean saved) {
            if(saved) {
                Toast.makeText(getApplicationContext(), "Updated user information", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Problem saving user information.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.play_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
