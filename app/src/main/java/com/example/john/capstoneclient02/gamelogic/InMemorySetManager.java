package com.example.john.capstoneclient02.gamelogic;

import com.example.john.capstoneclient02.Set;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by John on 11/21/2014.
 */
public class InMemorySetManager implements SetManager {

    List<Set> sets = new ArrayList<Set>();

    Integer indexOfCurrentSet;

    public void init() {
        sets.add(new Set("a", "b", "c", "1", "1", "not a letter", true));
        sets.add(new Set("D", "E", "F", "g", "g", "not an upper case letter", true));
        sets.add(new Set("1", "2", "3", "a", "a", "not a number", true));
        sets.add(new Set("cat", "car", "carrot", "man", "man", "does not start with the letter c", true));
        sets.add(new Set("oreo", "nutter butter", "fig newton", "starburst", "starburst", "not a cookie", true));
        sets.add(new Set("python", "ruby", "java", "whale", "whale", "not a computer language", true));
        sets.add(new Set("granite", "marble", "slate", "water", "water", "not a type of stone", true));
        sets.add(new Set("vw", "honda", "toyota", "nestle", "nestle", "not a car maker", true));
        sets.add(new Set("grouch", "bert", "ernie", "kermit", "kermit", "not on sesame street", true));
        sets.add(new Set("North", "South", "East", "down", "down", "not a compass heading", true));

        indexOfCurrentSet = 0;
    }

    public void setCurrentSet(Integer i) {
        indexOfCurrentSet = i;
    }

    public int getCurrentSetNumber() {
        return indexOfCurrentSet;
    }

    public String displayCurrentSet() {
        return "not needed";
    }

    public boolean isCorrectAnswer(String answer) {
        return sets.get(indexOfCurrentSet).getAnswer().equals(answer);
    }

    public boolean isCorrectAnswer(int i) {
        Set s;
        s = sets.get(indexOfCurrentSet);
        String userAnswer;
        switch (i) {
            case 1:
                userAnswer = s.getMovie1();
                break;
            case 2:
                userAnswer = s.getMovie2();
                break;
            case 3:
                userAnswer = s.getMovie3();
                break;
            case 4:
                userAnswer = s.getMovie4();
                break;
            default:
                userAnswer = "";
        }
        return userAnswer.equals(s.getAnswer());
    }

    public void nextSet() {
        indexOfCurrentSet++;
    }

    public String getChoice01() {
        return sets.get(indexOfCurrentSet).getMovie1();
    }

    public String getChoice02() {
        return sets.get(indexOfCurrentSet).getMovie2();
    }

    public String getChoice03() {
        return sets.get(indexOfCurrentSet).getMovie3();
    }

    public String getChoice04() {
        return sets.get(indexOfCurrentSet).getMovie4();
    }

    public String getExplanation() {
        return sets.get(indexOfCurrentSet).getExplanation();
    }

    public String getAnswer() {
        return sets.get(indexOfCurrentSet).getAnswer();
    }
}
