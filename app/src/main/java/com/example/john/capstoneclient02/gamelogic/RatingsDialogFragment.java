package com.example.john.capstoneclient02.gamelogic;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.john.capstoneclient02.R;

/**
 * Created by John on 11/23/2014.
 */
public class RatingsDialogFragment extends DialogFragment {

    Integer setNumber;
    String explanation;

    public RatingsDialogFragment() {
    }

    public static RatingsDialogFragment newInstance(Integer setNumber, String exp) {
        RatingsDialogFragment ratingsDialogFragment = new RatingsDialogFragment();

        Bundle args = new Bundle();
        args.putInt("setNumber", setNumber);
        args.putString("expl", exp);
        ratingsDialogFragment.setArguments(args);

        return ratingsDialogFragment;

    }

    public interface RatingsDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, Integer setNumber);
        public void onDialogNegativeClick(DialogFragment dialog, Integer setNumber);
    }

    RatingsDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (RatingsDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement RatingsDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.ratings_dialog, null);

        TextView textViewDialog = (TextView) dialogView.findViewById(R.id.textViewExplanation);
        TextView textViewSetNumber = (TextView) dialogView.findViewById(R.id.textViewSetNumber);

        this.setNumber = getArguments().getInt("setNumber");
        this.explanation = getArguments().getString("expl");

        textViewSetNumber.setText("Set " + this.setNumber);
        textViewDialog.setText(this.explanation);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder
                .setPositiveButton("Good explanation\n(Keep question)", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onDialogPositiveClick(RatingsDialogFragment.this, setNumber);
                    }
                })
                .setNegativeButton("Poor explanation\n(Remove question)", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onDialogNegativeClick(RatingsDialogFragment.this, setNumber);
                    }
                });

        builder.setView(dialogView);

        return builder.create();
    }
}