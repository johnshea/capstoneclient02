package com.example.john.capstoneclient02.gamelogic;

/**
 * Created by John on 11/21/2014.
 */
public class zzzSet {

    private long id;

    private String movie1;
    private String movie2;
    private String movie3;
    private String movie4;
    private String answer;
    private String explanation;

    public zzzSet() {
    }

    public zzzSet(String movie1, String movie2, String movie3, String movie4, String answer, String explanation) {
        super();
        this.movie1 = movie1;
        this.movie2 = movie2;
        this.movie3 = movie3;
        this.movie4 = movie4;
        this.answer = answer;
        this.explanation = explanation;
    }

    public String displaySet() {
        StringBuilder output = new StringBuilder();

        output.append("1.  " + this.movie1 + "\n");
        output.append("2.  " + this.movie2 + "\n");
        output.append("3.  " + this.movie3 + "\n");
        output.append("4.  " + this.movie4 + "\n");

        return output.toString();
    }

    public String getMovie1() {
        return movie1;
    }

    public void setMovie1(String movie1) {
        this.movie1 = movie1;
    }

    public String getMovie2() {
        return movie2;
    }

    public void setMovie2(String movie2) {
        this.movie2 = movie2;
    }

    public String getMovie3() {
        return movie3;
    }

    public void setMovie3(String movie3) {
        this.movie3 = movie3;
    }

    public String getMovie4() {
        return movie4;
    }

    public void setMovie4(String movie4) {
        this.movie4 = movie4;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}

