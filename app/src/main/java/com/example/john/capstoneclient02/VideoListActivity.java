package com.example.john.capstoneclient02;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

public class VideoListActivity extends Activity {

	protected ListView videoList_;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_list);

        videoList_ = (ListView) findViewById(R.id.videoList);

	}

	@Override
	protected void onResume() {
		super.onResume();
		
		refreshVideos();
	}

	private void refreshVideos() {
		final UserSvcApi svc = UserSvc.getOrShowLogin(this);

		if (svc != null) {
			CallableTask.invoke(new Callable<Collection<User>>() {

				@Override
				public Collection<User> call() throws Exception {
					return svc.getAllUsers();
				}
			}, new TaskCallback<Collection<User>>() {

				@Override
				public void success(Collection<User> result) {
					List<String> names = new ArrayList<String>();
					for (User v : result) {
						names.add(v.getUserid());
					}
					videoList_.setAdapter(new ArrayAdapter<String>(
							VideoListActivity.this,
							android.R.layout.simple_list_item_1, names));
				}

				@Override
				public void error(Exception e) {
					Toast.makeText(
							VideoListActivity.this,
							"Unable to fetch the video list, please login again.",
							Toast.LENGTH_SHORT).show();

					startActivity(new Intent(VideoListActivity.this,
							LoginScreenActivity.class));
				}
			});
		}
	}

}
