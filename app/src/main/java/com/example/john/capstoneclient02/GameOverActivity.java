package com.example.john.capstoneclient02;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class GameOverActivity extends Activity {

    String firstName;
    Integer currentLevel;
    Integer score;
    Integer round;
    Integer roundsToPlay;
    Integer strikesEarned;
    Integer strikesAllowed;

    TextView textViewStatus;
    TextView textViewStatus2;
    TextView textViewScore;
    Button btnReturnToMainScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        textViewStatus = (TextView) findViewById(R.id.textViewStatus_GameOverActivity);
        textViewStatus2 = (TextView) findViewById(R.id.textViewStatus2_GameOverActivity);
        textViewScore = (TextView) findViewById(R.id.textViewScore_GameOverActivity);

        Bundle bundle = this.getIntent().getExtras();

        firstName = bundle.getString("firstName");
        currentLevel = bundle.getInt("currentLevel");
        score = bundle.getInt("score");
        round = bundle.getInt("currentRound");
        roundsToPlay = bundle.getInt("roundsToPlay");
        strikesEarned = bundle.getInt("strikesEarned");
        strikesAllowed = bundle.getInt("maxStrikesAllowed");

        StringBuilder message1 = new StringBuilder();
        StringBuilder message2 = new StringBuilder();

        if (strikesAllowed == strikesEarned) {
            message1.append("Sorry " + firstName + ".");
            message2.append("Too many incorrect guesses.");
        } else {
            message1.append("Nice job " + firstName + "!");
            message2.append("You finished " + roundsToPlay + " rounds with " + strikesEarned + " incorrect");
            if (strikesEarned == 1) {
                message2.append(" guess.");
            } else {
                message2.append(" guesses.");
            }
        }

        textViewStatus.setText(message1.toString());
        textViewStatus2.setText(message2.toString());

        textViewScore.setText("Your final score was " + score + ".");

        btnReturnToMainScreen=(Button)findViewById(R.id.btnReturn_GameOverActivity);

        btnReturnToMainScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MyActivity.class);

                startActivity(intent);

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.game_over, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
