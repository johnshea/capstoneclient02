package com.example.john.capstoneclient02.gamelogic;

import android.os.UserManager;
import com.example.john.capstoneclient02.User;

/**
 * Created by John on 11/21/2014.
 */
public class Game {

    private int strikes;
    private SetManager setManager;
    private UserManager userManager;
    private User currentUser;

    public Game() {
    }

    public Game(int playerNumber, int numOfRounds) {

        //userManager = new UserManager();
        //userManager.init();

        //currentUser = userManager.getUser(playerNumber);

        setManager = new InMemorySetManager();
        setManager.init();

        System.out.print("User is starting on level " + currentUser.getCurrentLevel());
        setManager.setCurrentSet((int)(long)currentUser.getCurrentLevel());

        strikes = 3;
        boolean hasLives = true;
        int currentRound = 0;
        int possiblePoints = 100;

        int currentScore = 0;

        while (hasLives && currentRound < numOfRounds) {
            System.out.print("\n");
            System.out.print(setManager.displayCurrentSet());

            System.out.println("Possible points: " + possiblePoints);
            System.out.println("Enter 1 thru 4: ");

//            int answerNumber = Mutibo.in.nextInt();
            int answerNumber = 0;

            if(setManager.isCorrectAnswer(answerNumber)) {
                System.out.println("Correct!");
                currentScore += possiblePoints;
                System.out.println("Current score = " + currentScore);
                currentRound++;
                setManager.nextSet();
                possiblePoints = 100;
            } else {
                System.out.println("Wrong answer!");
                possiblePoints -= 25;
                strikes--;
                System.out.println("Strikes remaining: " + strikes);

                if (strikes==0) {
                    hasLives = false;
                }

            }


        }

        System.out.print("Game over");

        System.out.print("\nScore: " + currentScore);


//
//		Scanner in = new Scanner(System.in);
//
//		int i = -1;
//
//		while (i == -1) {
//			System.out.println("Enter integer for player number: ");
//
//			int j = in.nextInt();
//
////			in.close();
//
//			System.out.println(currentUser);
//
//			System.out.println("Enter -1 to continue");
//			i = in.nextInt();
////			in.close();
//
//		}
//		in.close();
//		System.out.println("Enter integer for player number: ");
//
//		int i = in.nextInt();
//
//		in.close();
//
//		currentUser = userManager.getUser(i);
//		System.out.println(currentUser);

//		System.out.println(setManager.displayCurrentSet());
//
//		System.out.println("nutter butter: " + setManager.isCorrectAnswer("nutter butter"));
//		System.out.println("1: " + setManager.isCorrectAnswer("1"));

    }

}
