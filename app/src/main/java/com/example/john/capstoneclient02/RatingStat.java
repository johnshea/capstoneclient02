package com.example.john.capstoneclient02;

/**
 * Created by John on 11/24/2014.
 */
public class RatingStat {

    private long setid;
    private long playedCount;
    private double averageRating;

    public RatingStat() {
    }

    public RatingStat(long setid, long playedCount, double averageRating) {
        super();
        this.setid = setid;
        this.playedCount = playedCount;
        this.averageRating = averageRating;
    }

    public long getSetid() {
        return setid;
    }

    public void setSetid(long setid) {
        this.setid = setid;
    }

    public long getPlayedCount() {
        return playedCount;
    }

    public void setPlayedCount(long playedCount) {
        this.playedCount = playedCount;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

}

