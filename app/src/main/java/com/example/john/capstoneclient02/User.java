package com.example.john.capstoneclient02;

//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;

import com.google.common.base.Objects;

//@Entity
public class User {

//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String userid;
	private String firstName;
	private String lastName;
	private long currentLevel;
	private long score;

	public User() {
	}

	public User(String userid, String firstName, String lastName, long currentLevel, long score) {
		super();
		this.userid = userid;
		this.firstName = firstName;
		this.lastName = lastName;
		this.currentLevel = currentLevel;
		this.score = score;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getCurrentLevel() {
		return currentLevel;
	}

	public void setCurrentLevel(long currentLevel) {
		this.currentLevel = currentLevel;
	}
	
	public Long getScore() {
		return score;
	}

	public void setScore(long score) {
		this.score = score;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Two Users will generate the same hashcode if they have exactly the same
	 * values for their nameLast, currentLevel, and currentPoints.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(userid, firstName, lastName, currentLevel, score);
	}

	/**
	 * Two Users are considered equal if they have exactly the same values for
	 * their nameFirst, nameLast, currentLevel, and currentPoints.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			User other = (User) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(userid, other.userid)
					&& Objects.equal(firstName, other.firstName)
					&& Objects.equal(lastName, other.lastName)
					&& Objects.equal(currentLevel, other.currentLevel)
					&& Objects.equal(score, other.score);
		} else {
			return false;
		}
	}

}
