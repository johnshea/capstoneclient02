package com.example.john.capstoneclient02;

import com.google.common.base.Objects;

/**
 * Created by John on 11/21/2014.
 */
public class Set {

    private long id;

    private String movie1;
    private String movie2;
    private String movie3;
    private String movie4;
    private String answer;
    private String explanation;
    private Boolean enabled;

    public Set() {
    }

    public Set(String movie1, String movie2, String movie3, String movie4, String answer, String explanation, Boolean enabled) {
        super();
        this.movie1 = movie1;
        this.movie2 = movie2;
        this.movie3 = movie3;
        this.movie4 = movie4;
        this.answer = answer;
        this.explanation = explanation;
        this.enabled = enabled;
    }

    public String getMovie1() {
        return movie1;
    }

    public void setMovie1(String movie1) {
        this.movie1 = movie1;
    }

    public String getMovie2() {
        return movie2;
    }

    public void setMovie2(String movie2) {
        this.movie2 = movie2;
    }

    public String getMovie3() {
        return movie3;
    }

    public void setMovie3(String movie3) {
        this.movie3 = movie3;
    }

    public String getMovie4() {
        return movie4;
    }

    public void setMovie4(String movie4) {
        this.movie4 = movie4;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    /**
     * Two Videos will generate the same hashcode if they have exactly the same
     * values for their name, url, and duration.
     *
     */
    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(movie1, movie2, movie3, movie4, answer, explanation);
    }

    /**
     * Two Videos are considered equal if they have exactly the same values for
     * their name, url, and duration.
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Set) {
            Set other = (Set) obj;
            // Google Guava provides great utilities for equals too!
            return Objects.equal(movie1, other.movie1)
                    && Objects.equal(movie2, other.movie2)
                    && Objects.equal(movie3, other.movie3)
                    && Objects.equal(movie4, other.movie4)
                    && Objects.equal(answer, other.answer)
                    && Objects.equal(explanation, other.explanation);
        } else {
            return false;
        }
    }

}
