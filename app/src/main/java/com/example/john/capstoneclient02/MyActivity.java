package com.example.john.capstoneclient02;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MyActivity extends Activity {

    Button btnLogin;
    Button btnStartGame;
    TextView textViewStatus;

    static final int LOGIN_REQUEST = 1;

    protected User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        btnLogin = (Button) findViewById(R.id.btnLogin_main);
        btnStartGame = (Button) findViewById(R.id.btnStartGame_main);
        textViewStatus = (TextView) findViewById(R.id.textViewStatus_main);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginScreenActivity.class);
                startActivityForResult(intent, LOGIN_REQUEST);

            }
        });

        btnStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PlayGameActivity.class);

                Bundle bundle = new Bundle();

                bundle.putLong("id", currentUser.getId());
                bundle.putString("userId", currentUser.getUserid());
                bundle.putString("firstName", currentUser.getFirstName());
                bundle.putString("lastName", currentUser.getLastName());
                bundle.putLong("currentLevel", currentUser.getCurrentLevel());
                bundle.putLong("score", currentUser.getScore());

                intent.putExtras(bundle);

                startActivity(intent);
            }
        });

        // Intent used for starting the MusicService
        final Intent musicServiceIntent = new Intent(getApplicationContext(),
                MusicService.class);

        final Button startButton = (Button) findViewById(R.id.start_button);

        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View src) {
            // Start the MusicService using the Intent
                startService(musicServiceIntent);
            }
        });

        final Button stopButton = (Button) findViewById(R.id.stop_button);
        stopButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View src) {
                // Stop the MusicService using the Intent
                stopService(musicServiceIntent);
            }
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOGIN_REQUEST) {

            boolean isLoggedIn = data.getBooleanExtra("isLoggedIn", false);

            if(isLoggedIn) {
                new myTask().execute();
                btnStartGame.setEnabled(true);
            } else {
                textViewStatus.setText("problem logging in");
                btnStartGame.setEnabled(false);
            }

        }
    }

    class myTask extends AsyncTask<Void, Void, User> {

        User u;

        @Override
        protected User doInBackground(Void... voids) {
            final UserSvcApi svc = UserSvc.getOrShowLogin(getApplicationContext());

            u = svc.getUserInfo();

            return u;
        }

        @Override
        protected void onPostExecute(User returnedUser) {
//            super.onPostExecute(aVoid);

            textViewStatus.setText("Welcome back " + returnedUser.getUserid()
            + "\nCurrent Level: " + returnedUser.getCurrentLevel()
            + "\nHigh Score: " + returnedUser.getScore());

            currentUser = returnedUser;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
