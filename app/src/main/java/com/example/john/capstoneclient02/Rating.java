package com.example.john.capstoneclient02;

/**
 * Created by John on 11/24/2014.
 */
public class Rating {

    private long id;

    private long setid;
    private long userid;
    private int rating;

    public Rating() {
    }

    public Rating(long setid, long userid, int rating) {
        super();
        this.setid = setid;
        this.userid = userid;
        this.rating = rating;
    }

    public long getSetid() {
        return setid;
    }

    public void setSetid(long setid) {
        this.setid = setid;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}

