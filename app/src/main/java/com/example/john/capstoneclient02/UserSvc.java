/* 
 **
 ** Copyright 2014, Jules White
 **
 ** 
 */
package com.example.john.capstoneclient02;

import android.content.Context;
import android.content.Intent;

import com.example.john.capstoneclient02.oauth.SecuredRestBuilder;
import com.example.john.capstoneclient02.unsafe.EasyHttpClient;

import retrofit.client.ApacheClient;

public class UserSvc {

	public static final String CLIENT_ID = "mobile";

	private static UserSvcApi userSvc_;

	public static synchronized UserSvcApi getOrShowLogin(Context ctx) {
		if (userSvc_ != null) {
			return userSvc_;
		} else {
			Intent i = new Intent(ctx, LoginScreenActivity.class);
			ctx.startActivity(i);
			return null;
		}
	}

	public static synchronized UserSvcApi init(String server, String user,
			String pass) {

//        .setLoginEndpoint(server + com.example.john.capstoneclient02.VideoSvcApi.TOKEN_PATH)

        userSvc_ = new SecuredRestBuilder()
				.setLoginEndpoint(server + "/login")
				.setUsername(user)
				.setPassword(pass)
				.setClientId(CLIENT_ID)
				.setClient(
						new ApacheClient(new EasyHttpClient()))
				.setEndpoint(server)
                //.setLogLevel(LogLevel.FULL)
                .build()
				.create(UserSvcApi.class);

		return userSvc_;
	}
}
