package com.example.john.capstoneclient02;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Collection;
import java.util.concurrent.Callable;

public class LoginScreenActivity extends Activity {

	protected EditText userName_;
	protected EditText password_;
	protected EditText server_;
    protected Button btnLogin;

    protected Button btnReturnToMainScreen;

    protected boolean isLoggedIn = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_screen);

        btnLogin = (Button) findViewById(R.id.loginButton);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        btnReturnToMainScreen=(Button)findViewById(R.id.btnReturn);

        btnReturnToMainScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("isLoggedIn", isLoggedIn);

                setResult(RESULT_OK, intent);

                finish();

            }
        });

        Animation button_spin = AnimationUtils.loadAnimation(this, R.anim.spin_anim);
        btnReturnToMainScreen.startAnimation(button_spin);

	}

	public void login() {

        userName_ = (EditText) findViewById(R.id.userName);
        password_ = (EditText) findViewById(R.id.password);
        server_ = (EditText) findViewById(R.id.server);

		String user = userName_.getText().toString();
		String pass = password_.getText().toString();
		String server = server_.getText().toString();

		final UserSvcApi svc = UserSvc.init(server, user, pass);

		CallableTask.invoke(new Callable<Collection<User>>() {

			@Override
			public Collection<User> call() throws Exception {
				return svc.getAllUsers();
			}
		}, new TaskCallback<Collection<User>>() {

			@Override
			public void success(Collection<User> result) {
				// OAuth 2.0 grant was successful and we
				// can talk to the server, open up the video listing
                isLoggedIn = true;
//				startActivity(new Intent(
//						LoginScreenActivity.this, VideoListActivity.class));
                Toast.makeText(
                        LoginScreenActivity.this,
                        "Login success",
                        Toast.LENGTH_SHORT).show();

                Intent intent = new Intent();
                intent.putExtra("isLoggedIn", isLoggedIn);

                setResult(RESULT_OK, intent);

                finish();
			}

			@Override
			public void error(Exception e) {
				Log.e(LoginScreenActivity.class.getName(), "Error logging in via OAuth.", e);
				
				Toast.makeText(
						LoginScreenActivity.this,
						"Login failed, check your Internet connection and credentials.",
						Toast.LENGTH_SHORT).show();
			}
		});
	}

}
