package com.example.john.capstoneclient02.gamelogic;

/**
 * Created by John on 11/22/2014.
 */
public class GameStateManager {

    private int maxStrikesAllowed;
    private int strikesEarned;
    private int fullPointsForCorrectAnswer;
    private int pointsDeductedForWrongAnswer;
    private int currentScore;
    private int currentPointsValue;
    private int roundsToPlay;
    private int currentRound;

    public GameStateManager() {
        this.maxStrikesAllowed = 3;
        this.strikesEarned = 0;
        this.fullPointsForCorrectAnswer = 100;
        this.pointsDeductedForWrongAnswer = 25;
        this.currentScore = 0;
        this.currentPointsValue = this.fullPointsForCorrectAnswer;
        this.roundsToPlay = 8;
        this.currentRound = 1;
    }

    public int getMaxStrikesAllowed() {
        return maxStrikesAllowed;
    }

    public void setMaxStrikesAllowed(int maxStrikesAllowed) {
        this.maxStrikesAllowed = maxStrikesAllowed;
    }

    public int getFullPointsForCorrectAnswer() {
        return fullPointsForCorrectAnswer;
    }

    public void setFullPointsForCorrectAnswer(int fullPointsForCorrectAnswer) {
        this.fullPointsForCorrectAnswer = fullPointsForCorrectAnswer;
    }

    public int getPointsDeductedForWrongAnswer() {
        return pointsDeductedForWrongAnswer;
    }

    public void setPointsDeductedForWrongAnswer(int pointsDeductedForWrongAnswer) {
        this.pointsDeductedForWrongAnswer = pointsDeductedForWrongAnswer;
    }

    public int getStrikesEarned() {
        return strikesEarned;
    }

    public void setStrikesEarned(int strikesEarned) {
        this.strikesEarned = strikesEarned;
    }

    public void incrementStrikesEarned() {
        this.strikesEarned++;
    }

    public void decrementPointValue() {
        this.currentPointsValue -= this.pointsDeductedForWrongAnswer;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    public int getCurrentPointsValue() {
        return currentPointsValue;
    }

    public void setCurrentPointsValue(int currentPointsValue) {
        this.currentPointsValue = currentPointsValue;
    }

    public void incrementScore() {
        this.currentScore += this.currentPointsValue;
    }

    public void resetPossiblePoints() {
        this.currentPointsValue = this.fullPointsForCorrectAnswer;
    }

    public int getRoundsToPlay() {
        return roundsToPlay;
    }

    public void setRoundsToPlay(int roundsToPlay) {
        this.roundsToPlay = roundsToPlay;
    }

    public void incrementRound() {
        this.currentRound++;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public boolean isGameOver() {
        if ((this.strikesEarned >= this.maxStrikesAllowed) || (this.currentRound >= this.roundsToPlay)) {
            return true;
        } else {
            return false;
        }
    }
}
