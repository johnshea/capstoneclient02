/* 
**
** Copyright 2014, Jules White
**
** 
*/
package com.example.john.capstoneclient02;

public interface TaskCallback<T> {

    public void success(T result);

    public void error(Exception e);

}
