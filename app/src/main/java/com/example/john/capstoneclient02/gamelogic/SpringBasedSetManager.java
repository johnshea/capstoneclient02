package com.example.john.capstoneclient02.gamelogic;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.example.john.capstoneclient02.Set;
import com.example.john.capstoneclient02.UserSvc;
import com.example.john.capstoneclient02.UserSvcApi;

/**
 * Created by John on 11/25/2014.
 */
public class SpringBasedSetManager implements SetManager {

    int indexOfCurrentSet;

    Set currentSet;
    Context ctx;
    Handler handler;

    public SpringBasedSetManager(Context ctx, Handler handler) {
        this.ctx = ctx;
        this.handler = handler;
    }

    public void init() {

        RetrieveSetTask retrieveSetTask = new RetrieveSetTask(ctx);

        retrieveSetTask.execute(1);

        return;

    }

    public void setCurrentSet(Integer i) {
        indexOfCurrentSet = i;

        RetrieveSetTask retrieveSetTask = new RetrieveSetTask(ctx);

        retrieveSetTask.execute(indexOfCurrentSet);
    }

    public int getCurrentSetNumber() {
        return (int)currentSet.getId();
    }

    public String displayCurrentSet() {
        return "SpringBasedSetManager.displayCurrentSet - probably not needed";
//        sets.get(indexOfCurrentSet).displaySet();
    }

    public boolean isCorrectAnswer(String answer) {
        return currentSet.getAnswer().equals(answer);
    }

    public boolean isCorrectAnswer(int i) {
        Set s;
        s = currentSet;
        String userAnswer;
        switch (i) {
            case 1:
                userAnswer = s.getMovie1();
                break;
            case 2:
                userAnswer = s.getMovie2();
                break;
            case 3:
                userAnswer = s.getMovie3();
                break;
            case 4:
                userAnswer = s.getMovie4();
                break;
            default:
                userAnswer = "";
        }
        return userAnswer.equals(s.getAnswer());
    }

    public void nextSet() {

        RetrieveNextEnabledSetTask retrieveNextEnabledSetTask = new RetrieveNextEnabledSetTask(ctx);

        retrieveNextEnabledSetTask.execute((int)currentSet.getId());
    }

    public String getChoice01() {
        return currentSet.getMovie1();
    }

    public String getChoice02() {
        return currentSet.getMovie2();
    }

    public String getChoice03() {
        return currentSet.getMovie3();
    }

    public String getChoice04() {
        return currentSet.getMovie4();
    }

    public String getExplanation() {
        return currentSet.getExplanation();
    }

    public String getAnswer() {
        return currentSet.getAnswer();
    }

    public class RetrieveSetTask extends AsyncTask<Integer, Void, Void> {

        Context ctx;

        public RetrieveSetTask(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        protected Void doInBackground(Integer... setId) {

            final UserSvcApi svc = UserSvc.getOrShowLogin(this.ctx);

            currentSet = svc.getOneSet(setId[0]);

            if (currentSet.getEnabled().equals(false)) {
                currentSet = svc.getNextEnabledSet(setId[0]);
            }

            indexOfCurrentSet = (int)currentSet.getId();

            Message msg = handler.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putInt("status", 0);
            msg.setData(bundle);
            handler.sendMessage(msg);

            return null;
        }

    }

    public class RetrieveNextEnabledSetTask extends AsyncTask<Integer, Void, Void> {

        Context ctx;

        public RetrieveNextEnabledSetTask(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        protected Void doInBackground(Integer... setId) {

            final UserSvcApi svc = UserSvc.getOrShowLogin(this.ctx);

            currentSet = svc.getNextEnabledSet(setId[0]);

            Message msg = handler.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putInt("status", 1);
            msg.setData(bundle);
            handler.sendMessage(msg);

            return null;
        }

    }

}

