package com.example.john.capstoneclient02.gamelogic;

/**
 * Created by John on 11/24/2014.
 */
public interface SetManager {
    public void init();
    public void setCurrentSet(Integer i);
    public int getCurrentSetNumber();
    public String displayCurrentSet();
    public boolean isCorrectAnswer(String answer);
    public boolean isCorrectAnswer(int i);
    public void nextSet();
    public String getChoice01();
    public String getChoice02();
    public String getChoice03();
    public String getChoice04();
    public String getExplanation();
    public String getAnswer();
}
