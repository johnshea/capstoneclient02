
package com.example.john.capstoneclient02;

import java.util.Collection;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by John on 11/15/2014.
 */
public interface UserSvcApi {

    @GET("/user/info")
    User getUserInfo();

    @GET("/user")
    Collection<User> getAllUsers();

    @GET("/set/{id}")
    Set getOneSet(@Path("id") long id);

    @GET("/set/after/{id}")
    Set getNextEnabledSet(@Path("id") long id);

    @POST("/user/info")
    Boolean setInfo(@Body User s);

    @POST("/rating")
    Boolean addRating(@Body Rating r);

    @GET("/rating/stats/{id}")
    RatingStat getOneRatingStat(@Path("id") long id);

    @POST("/set/disable/{id}")
    Boolean disableOneSet(@Path("id") long id);

}
